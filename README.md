# BSA Repository

This is a utility repostiory for portmod that profiles a BSA class which can be used to install mods on vanilla Bethesda Engines making use of BSA archives as a virtual file system.

All possible mod assets get packed into the BSA, allowing multiple mods to install the same assets without causing file conflicts.

This currently requres the use of a work-in-progress version of bsatool. See https://gitlab.com/OpenMW/openmw/-/merge_requests/783
